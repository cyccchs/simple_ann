#include <ann.h>
#include <iostream>

node::node(int input_num, int output_num): input_dim(input_num), output_dim(output_num)
{
    this->weight = Eigen::VectorXd(input_dim);
    this->weight.setRandom();
    cout << "node with weight : " << this->weight << endl;
}
void node::forward(Eigen::VectorXd x)
{
    Eigen::VectorXd tmp(input_dim);
    double sum = 0;
    for(int i=0; i<x.size(); i++){
        tmp(i) = this->weight(i)*x(i);
        sum += tmp(i);
    }
    this->output_value = sum/this->input_dim;
    this->output_value = (this->output_value > 0) ? this->output_value : 0;
}
double node::get_output_value()
{
    return this->output_value;
}

nn::layer::layer(){}
void nn::layer::create(int input, int output)
{
    for(int i=0; i<input; i++)
        nodes.push_back(node(input, output));
    cout << "layer size: " << nodes.size() << endl;
}

Eigen::VectorXd nn::layer::forward(Eigen::VectorXd input)
{
    Eigen::VectorXd result(input.size());
    for(int i=0; i<(int)nodes.size(); i++){
        nodes[i].forward(input);
        result(i) = nodes[i].get_output_value();
        nodes[i].show_output();
    }
    return result;
}

nn::nn(double learning_rate): lr(learning_rate){}
void nn::layer_append(int input, int output)
{
    layer* ptr = new layer;
    ptr->create(input, output);
    layers.push_back(*ptr);
}
Eigen::VectorXd nn::forward(int index, Eigen::VectorXd x)
{
       return this->layers[index].forward(x);
}

void node::show_weight(){cout << this->weight << endl;}
void node::show_output(){cout << this->output_value << endl;}

