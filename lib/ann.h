#include <Eigen/Eigen>
#include <unsupported/Eigen/CXX11/Tensor>
#include <vector>

using namespace std;

class node {
    public:
        node(int input_num, int output_num);
        void forward(Eigen::VectorXd x);
        double get_output_value();
        void show_weight();
        void show_output();
    private:
        int output_dim;
        int input_dim;
        Eigen::VectorXd weight;
        double output_value;
};

class nn{
    private:
        class layer{
            public:
                layer();
                void create(int input, int output);
                Eigen::VectorXd forward(Eigen::VectorXd input);
            private:
                vector<node> nodes;
        };
        float lr;
        vector<layer> layers;
    public:
        nn(double learning_rate);
        void layer_append(int input, int output);
        Eigen::VectorXd forward(int index, Eigen::VectorXd x);
};
