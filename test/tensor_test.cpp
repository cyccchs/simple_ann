#include <ann.h>
#include <iostream>

using namespace std;
using namespace Eigen;

int main(void){
    Tensor <float, 5> tensor_nd(1, 3, 2, 1, 4);
    tensor_nd.setZero();
    cout << "All zero:\n" << tensor_nd << endl;
    tensor_nd.setConstant(1.0f);
    cout << "All one:\n " << tensor_nd << endl;
    tensor_nd.setRandom();
    cout << "All random:\n" << tensor_nd << endl;
    
    cout << "tensor dimension: " << tensor_nd.NumDimensions << endl;
    cout << "tensor size: " << tensor_nd.size() << endl;
    cout << "length of dimension 5: " << tensor_nd.dimension(4) << endl;

    Tensor <float, 2> a(3, 3);
    a.setRandom();
    Tensor <float, 2> b(3, 3);
    b.setRandom();

    cout << a << "\n\n";
    cout << b << "\n\n";

    cout << a*b << "\n\n";
    

    return 0;
}
