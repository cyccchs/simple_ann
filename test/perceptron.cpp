#include <iostream>
#include <ann.h>

int main(void)
{
    Eigen::Vector3d input(0.5,1.5,2);
    Eigen::VectorXd x;
    
    nn foo(0.1);

    foo.layer_append(3,16);
    foo.layer_append(16, 4);
    
    x = foo.forward(0, input);

    return 0;
}
