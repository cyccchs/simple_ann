#include <iostream>
#include <ann.h>

using namespace std;
using namespace Eigen;

int main(void)
{
    cout << "This is Eigen3 test !" << endl;

    MatrixXd m(2, 2);
    m(0, 0) = 3;
    m(0, 1) = 2.5;
    m(1, 0) = -1;
    m(1, 1) = m(1,0) + m(0,1);

    cout << m(1, 0) << "+" << m(0, 1) << "=" << m(1, 1) << endl;
    return 0;
}
